from tutorial.settings import settings


###########################################################################################
# This is a master function that will help make sure the values that are being passed in
# are valid and can be handled appropriately.
#
# INPUT
# 1) The Request Object - This has all of the input variables
# 2) The argument name (i.e. email, first_name, etc.)  This helps to grab the right data from object
# 3) required.  True or False to determine if the field is required or not
# 4) Field Name.  Human Readable name of field.  Helps with error messaging and logging) 
# 5) Validation Type = Multiple Validation types (email, password, integer only, etc.  Helps identify the rules used
# 6) Max Value = the maximum value for a given field
# 7) Request Type = Get or POST.  Helps us determine how to get arguments
# 8) Global Object = Need the global object to keep state as we collect validation rules
#
# OUTPUT
#  Will return the value passed in (url decoded and trimmed)
#  If no value is passed in, then it will return None
#
# ERROR HANDLING
# If validation fails, an error will be raised
#
############################################################################################
def validate_and_get_value(request_object, arg, required, field_name, validation_type,  max_value,  request_type, global_object):
	
	validation_error = None #assume there will be no validation error
	
	#Get the value from request object
	value = get_value_from_request(request_object, arg, request_type)
	
	#If field is required, then make sure we have it
	if (required):
		if (value == None):
			validation_error = field_name + " is a required field"
			
		
	#Make sure field is valid (different for each condition)
	if (validation_error == None):
		if (validation_type == settings.VALIDATION_TYPE_EMAIL):  #Check if email is valid
			if (not is_email_valid(value)):
				validation_error = field_name + " must be a valid email address"
		
	
	#Make sure max length is honored
	if ((validation_error == None) and (value != None) and (max_value != settings.NO_MAX_VALUE)):
		if (len(value) > max_value):
			validation_error = field_name + " must be less than " + str(max_value) + " characters"

			
	if (validation_error != None):
		global_object.api_response.validation_error[field_name] = validation_error
	
	
	return value


##############################################################
# This determines if the validation has passed or not.  Returns True or False
# If False, it sets the proper error code for response object
###############################################################
def validation_check(global_object):
	
	if (not global_object.api_response.validation_error):
		is_valid = True
	else:
		is_valid = False
		global_object.api_response.request_status_code = settings.STATUS_CODE_BAD_PARAMETER
		
	return is_valid


###################################################
# Gets the value coming from the request object.  
# Returns the value or None if empty
####################################################
def get_value_from_request(request_object, arg, request_type):
	try:
		#Get the value from the object
		if (request_type == settings.REQUEST_TYPE_GET):
			new_value = request_object.get(arg)
		elif (request_type == settings.REQUEST_TYPE_POST):
			new_value = request_object.post(arg)
	
		#Trim Value
		new_value = new_value.strip()
	
		#if we don't get a value, make it None
		if (new_value == ""):
			new_value = None	
	except:
		new_value = None	
	
	return new_value
	

###################################################
# Checks to see if email format is valid
# Note - does not check if email actually exists
# just checks if general syntax is valid
# Returns true if valid, false otherwise
####################################################
def is_email_valid(email):
	import re

	if len(email) > 7:
		if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
			return True
	return False



###################################################
# Checks to see if PASSWORD is valid
####################################################
def is_password_valid(password):
	x = "hello" #TO DO
	return True


##################################################################################################
# Checks to see if passed in arguments are in range of acceptable values.  if not, return default
#######################################################################################
def get_value_within_range(input_value, default_value, value_range):
	#Lets assume final value is the default unless told otherwise
	final_value = default_value
	
	#Loop through range of acceptable values and see if the input_value matches.  If yes, set.  Else, keep default_value
	for key, value in value_range:
		if (input_value == key):
			final_value = value
	
	return final_value


##################################################################################################
# Gets the mime type based on the output tpye
#######################################################################################
def get_mime_type(output_type):
	return_mime_type = settings.MIME_TYPE_JSON  #assume JSON unless otherwise stated
	
	if (output_type == "json"):
		return_mime_type = settings.MIME_TYPE_JSON
	
	if (output_type == "jsonp"):
		return_mime_type = settings.MIME_TYPE_JSONP
	
	return return_mime_type

