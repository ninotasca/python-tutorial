from tutorial.settings import settings

def get_database_collection(db_client, object_name):

	#Creates a dictionary that maps objects name in code to collection/table names in the database
	 database_dict = {
	 	"user": settings.DATABASE_COLLECTION_USERS,
	 	"blog": "blog_database"
	 }
	 
	 #create collection object 
	 db_collection = db_client[database_dict[object_name]]
	 
	 return db_collection