from tutorial.users import user
from tutorial.settings import settings

def find_user(global_object, querystring):
	
	#instatiate
	this_user = None
	
	from tutorial.functions.validation import validate_and_get_value, validation_check
	email 		= validate_and_get_value(querystring, "email", True, "email", settings.VALIDATION_TYPE_EMAIL, settings.NO_MAX_VALUE, settings.REQUEST_TYPE_GET, global_object)

	if (validation_check(global_object)):
		try:
			this_user = user.get_user(email, global_object.db)
		except Exception as e:
			this_user = None
			global_object.api_response.request_status_code = settings.STATUS_CODE_INTERNAL_SERVER_ERROR
			global_object.api_response.verbose_error = e.args
			
		else:
			global_object.api_response.request_data = this_user
			
	#Scrub User information so it is clean
	this_user = user.scrub_user(this_user)
	
	#Return the request object with all detailed information
	return this_user


def add_user(global_object, querystring):

	#instatiate user object
	this_user = None
	
	#get and validate all incoming fields
	from tutorial.functions.validation import validate_and_get_value, validation_check
	email 		= validate_and_get_value(querystring, "email", True, "email", settings.VALIDATION_TYPE_EMAIL, settings.NO_MAX_VALUE, settings.REQUEST_TYPE_GET, global_object)
	password 	= validate_and_get_value(querystring, "password", True, "password", settings.VALIDATION_TYPE_PASSWORD, settings.PASSWORD_MAX_VALUE, settings.REQUEST_TYPE_GET, global_object)
	
	
	if (validation_check(global_object)):
		try:
			user_object = user.create_user_object(email, password)	
			this_user = user.add_user(user_object, global_object.db)
		except Exception as e:
			global_object.api_response.request_status_code = settings.STATUS_CODE_INTERNAL_SERVER_ERROR
			global_object.api_response.verbose_error = e.args		
		else:
			global_object.api_response.request_data = this_user
	
	
	return this_user



def delete_user(global_object, querystring):

	#instatiate user object
	this_user = None
	
	#get and validate all incoming fields
	from tutorial.functions.validation import validate_and_get_value, validation_check
	id 		= validate_and_get_value(querystring, "id", True, "id", settings.VALIDATION_TYPE_ID, settings.NO_MAX_VALUE, settings.REQUEST_TYPE_GET, global_object)
	
	
	if (validation_check(global_object)):
		try:	
			this_user = user.delete_user(id, global_object.db)
		except Exception as e:
			global_object.api_response.request_status_code = settings.STATUS_CODE_INTERNAL_SERVER_ERROR
			global_object.api_response.verbose_error = e.args		
		else:
			global_object.api_response.request_data = this_user
	
	
	return this_user


#******** Returns a count of 
def user_count(global_object):
	
	user_count = 0 #assume zero unless stated otherwise
	
	try:
		user_count = user.count_users(global_object.db)
	except Exception as e:
		global_object.api_response.request_status_code = settings.STATUS_CODE_INTERNAL_SERVER_ERROR
		global_object.api_response.verbose_error = e.args
	
	else:
		return_count = { "count" : user_count }
		global_object.api_response.request_data = return_count
		