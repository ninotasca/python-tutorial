import json
from tutorial.functions.database import get_database_collection


def create_user_object(email, password):
	user_object = {"email": email, "password": password}
	return user_object



#************************************************************
# Gets a User Collection from the database and returns it
#************************************************************
def get_user(email, db_client):

	try:
		user_collection = get_database_collection(db_client, "user")
		this_user = user_collection.find_one({"email":email})
	except Exception as e:
		this_user = None
		raise
	
	return this_user
	
	
#************************************************************
# Gets a count of all entities in User collection
#************************************************************
def count_users(db_client):

	try:
		user_collection = get_database_collection(db_client, "user")
		this_count = user_collection.count()
	except Exception as e:
		this_count = 0
		raise
	
	return this_count


#************************************************
# Get a collection of Users and scrubs all sensitive information (i.e. password, credit card, etc.)
# before sending it back
#************************************************
def scrub_user(user):
	
	try:
		if (user != None):
			user.pop('password', None)
	except Exception as e:
		x = "something"
		#do nothing
	
	return user
	
	

#************************************************************
# Gets a User Collection from the database and returns it
#************************************************************
def add_user(user_object, db_client):

	try:
		user_collection = get_database_collection(db_client, "user")
		this_user = user_collection.insert(user_object)
	except Exception as e:
		this_user = None
		raise
	
	return this_user


#************************************************************
# Gets a User Collection from the database and returns it
#************************************************************
def delete_user(id, db_client):

	try:
		user_collection = get_database_collection(db_client, "user")

		#Need to convert ID to a BSON object
		from bson.objectid import ObjectId
		this_user = user_collection.remove({'_id':ObjectId(id)})
	except Exception as e:
		this_user = None
		raise
	
	return this_user
	
	
