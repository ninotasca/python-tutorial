import json, time
from bson import json_util
from tutorial.settings import settings

class JsonResponse:

	def __init__(self, request_start_time, output_type, verbose):
		self.request_status_code = settings.STATUS_CODE_SUCCESS #assume success unless otherwise indicated.  Will help us going forward
		self.request_data = None
		self.request_custom_message = None
		self.verbose = verbose
		self.output_type = output_type
		self.verbose_error = None
		self.validation_error = {}
		self.start_time = request_start_time
		self.end_time = None


	
	#*********************************************************************
	# Wraps up all of the appropriate data and sends in a message that is properly formatted
	#********************************************************************
	def request_response(self, end_time=0):
	
		#Determine the time it took to process request
		if (end_time == 0):
			end_time = int(round(time.time() * 1000))
		self.end_time = end_time
		total_time = str(self.end_time - self.start_time) + "ms"
		
		
		if (self.request_custom_message == None):
			request_message = self.request_status_code[2]
		else:
			request_message = self.request_custom_message
		
		return_json = {"status": self.request_status_code[1], "message": request_message, "data": self.request_data, "response_time": total_time}
		

		if (self.validation_error):
			return_json["validation_error"] = self.validation_error
		
		#If we are requesting verbose error, make sure we send that info
		if (self.verbose):
			return_json["error_message"] = self.verbose_error
		
		
		#Dump all information to a readable JSON file
		return_json = json.dumps(return_json, default=json_util.default)

		if self.output_type == "jsonp":
			return_json = "jsonCallback(" + return_json + ");"
		
		return return_json

