#****   VERY IMPORTANT  ***************
#  Do Not include this in packages when pushing to other environments
# This should be set once and only once per environment

ENVIRONMENT = "DEV"