
#determine which environment this code is running in
from tutorial.settings import environment_declaration
ENVIRONMENT_NAME = environment_declaration.ENVIRONMENT

#based on the environment that was declared, import the correct settings file
if (ENVIRONMENT_NAME == "PROD"):
	from tutorial.settings import settings_prod as env
else:
	from tutorial.settings import settings_dev as env




########## START - GLOBAL SETTINGS #######################################

DATABASE_COLLECTION_USERS = "AppUsers"



RANGE_POSSIBLE_OUTPUT_TYPES = [["json", "json"], ["jsonp", "jsonp"]]
RANGE_TRUE_FALSE = [["true", True], ["false", False]]


# Get all Status Codes here... http://www.flaskapi.org/api-guide/status-codes/
RESPONSE_STATUS_SUCCESS 	= "success"
RESPONSE_STATUS_FAIL 		= "fail"

STATUS_CODE_SUCCESS					= [200, RESPONSE_STATUS_SUCCESS, None]
STATUS_CODE_INTERNAL_SERVER_ERROR 	= [500, RESPONSE_STATUS_FAIL, "There was an internal server error.  Please try again or contact administrator"]
STATUS_CODE_NO_DATABASE_CONNECTION 	= [500, RESPONSE_STATUS_FAIL, "There was a problem connecting to the database.  Please try again or contact administrator"]
STATUS_CODE_UNAUTHORIZED 			= [401, RESPONSE_STATUS_FAIL, "Authentication is rquired"]
STATUS_CODE_PERMISSION_DENIED 		= [403, RESPONSE_STATUS_FAIL, "You do not have permission to access this data"]
STATUS_CODE_BAD_PARAMETER			= [400, RESPONSE_STATUS_FAIL, "The parameters passed in are not valid"]
STATUS_CODE_BAD_URL					= [404, RESPONSE_STATUS_FAIL, "The URL entered is not valid"]

MIME_TYPE_JSON		= "application/json"
MIME_TYPE_JSONP		= "application/javascript"




VALIDATION_TYPE_EMAIL 		= "vt_email" 
VALIDATION_TYPE_PASSWORD 	= "vt_password" 
VALIDATION_TYPE_ID 			= "vt_id" 

NO_MAX_VALUE				= -1
PASSWORD_MAX_VALUE			= 10

REQUEST_TYPE_GET			= "GET"
REQUEST_TYPE_POST			= "POST"

########## END - GLOBAL SETTINGS #######################################




############## START - ENVIRONMNET SETTINGS ########################
# NOTE - this may seem a bit unnecessary, but it ensures an extra ounce of redundancy that should ensure we are careful with these
# It is also important that the app doesn't need to know which fields are global vs environment

DATABASE_LOCATION = env.E_DATABASE_LOCATION
DATABASE_CLIENT = env.E_DATABASE_CLIENT


OUTPUT_TYPE_DEFAULT = env.E_OUTPUT_TYPE_DEFAULT
VERBOSE_ERROR_DEFAULT = env.E_VERBOSE_ERROR_DEFAULT
DATA_ONLY_OUTPUT_DEFAULT = env.E_DATA_ONLY_OUTPUT_DEFAULT

############## END - ENVIRONMNET SETTINGS ########################
