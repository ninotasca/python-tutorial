from flask import Flask, request, g, abort
app = Flask(__name__)

import urllib, time, json
from pymongo import MongoClient
from bson import json_util

#from tutorial import settings
from tutorial.settings import settings

from tutorial import json_response
from tutorial.users import user_views
from tutorial.sandbox import sandbox_views


#*********** calls that happen before and after each request *************
@app.before_request
def before_request():
	request_start_time = int(round(time.time() * 1000))
	
	#Get the global output type & verbose-ness
	from tutorial.functions.validation import get_value_within_range
	output_type        = get_value_within_range(request.args.get('output'), settings.OUTPUT_TYPE_DEFAULT, settings.RANGE_POSSIBLE_OUTPUT_TYPES) 
	verbose            = get_value_within_range(request.args.get('verbose'), settings.VERBOSE_ERROR_DEFAULT, settings.RANGE_TRUE_FALSE) 
	return_data_only   = get_value_within_range(request.args.get('data_only'), settings.DATA_ONLY_OUTPUT_DEFAULT, settings.RANGE_TRUE_FALSE) 	
	g.return_data_only = return_data_only
	g.output_type 	   = output_type
		

	#Insatiate the response object 
	g.api_response = json_response.JsonResponse(request_start_time, output_type, verbose)
	
	#For pings, ignore database connection
	if (request.path != "/ping"):
		#Create Connection to Database
		try:
			g.client = MongoClient(settings.DATABASE_LOCATION)
			g.db = g.client[settings.DATABASE_CLIENT]	
		except Exception as e:
			g.api_response.request_status_code = settings.STATUS_CODE_NO_DATABASE_CONNECTION
			g.api_response.verbose_error = e.args
			abort(g.api_response.request_status_code[0])
	
	
	

@app.after_request
def after_request(response):
	
	#Check to make sure that this is not a 404 request
	if (response.status_code == 404):
		g.api_response.request_status_code = settings.STATUS_CODE_BAD_URL

	if (g.return_data_only == False):
		request_end_time = int(round(time.time() * 1000))
		response.data = g.api_response.request_response(request_end_time)   # Why can't I do this - it would be more efficient - but it seems wrong
	
	#set the response mime type and status appropriately
	from tutorial.functions.validation import get_mime_type
	response.status_code = g.api_response.request_status_code[0]
	response.mimetype = get_mime_type(g.output_type)
	
	return response
	
	
	
@app.teardown_request 
def teardown_request(response):
	#Close all remaining database connections
	try:
		g.client.disconnect()
	except:
		x="something"  #I need to log that we couldn't kill connection





	


#***************** USERS ************************
@app.route('/user/find')
def route_find_user():
	try:
		this_user = user_views.find_user(g, request.args)
	except:
		this_user = None
	
	return json.dumps(this_user, default=json_util.default)
	
	
	
@app.route('/user/add')
def route_user_add():
	try:
		this_user = user_views.add_user(g, request.args)
	except:
		this_user = None
	
	return json.dumps(this_user, default=json_util.default)


@app.route('/user/delete')
def route_user_delete	():
	try:
		this_user = user_views.delete_user(g, request.args)
	except:
		this_user = None
	
	return json.dumps(this_user, default=json_util.default)
	
	
	
#***************** PINTG *************************
# Easy way to make sure that service is up and running
#*************************************************	
@app.route('/ping')
@app.route('/ping-db-connection')
def route_ping():
	try:
		g.api_response.request_status_code = settings.STATUS_CODE_SUCCESS
		current_time = time.time()
		return_ping = { "ping_time": current_time}
		g.api_response.request_data = return_ping
	except:
		g.api_response.request_status_code = settings.STATUS_CODE_INTERNAL_SERVER_ERROR
		return_ping = None
		
	return json.dumps(return_ping, default=json_util.default)
	


@app.route('/ping-db-read')
def route_ping_db_read():
	try:
		this_user = user_views.user_count(g)
	except:
		this_user = None
	
	return json.dumps(this_user, default=json_util.default)

	
	
	
#***************** SANDBOX *************************
# This allows us to easily try things without getting too caught up in correct code
#*************************************************
@app.route('/sandbox')
def get_sandbox():
	#if (g.api_response.request_status == "success"):
	#	user = sandbox_views.sandbox(g)
	#else:
	#	user = None
	
	user = settings.ENVIRONMENT_NAME
	return json.dumps(user, default=json_util.default)

		
		

	
